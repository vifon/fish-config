if status is-interactive
    abbr -a -- c ' cd'
    abbr -a -- l ' ls'
    abbr -a -- g ' git'
    abbr -a -- r ' rifle'
end
