if status is-interactive; and command -q fzf
    fzf --fish | source
end
