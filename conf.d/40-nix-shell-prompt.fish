if set -q IN_NIX_SHELL
    set --prepend fish_before_prompt "(nix-shell) "
end
