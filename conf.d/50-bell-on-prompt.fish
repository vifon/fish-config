if status is-interactive
    function __postexec_bell --on-event fish_postexec
        printf '\a'
    end
end
