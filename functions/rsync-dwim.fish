function rsync-dwim --wraps=rsync
    rsync -avhxX --info=progress2 $argv
end
