if status is-interactive
    if command -q eza
        function ls --description 'List contents of directory' --wraps=eza
            set -l param --group-directories-first --group
            command eza $param $argv
        end
    else
        function ls --description 'List contents of directory' --wraps=ls
            set -l param --color=auto --group-directories-first
            command ls $param $argv
        end
    end
end
